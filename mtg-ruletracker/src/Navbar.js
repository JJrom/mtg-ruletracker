import React, { Component } from 'react'
import { Button, Navbar, Nav, NavDropdown, Form, FormControl } from 'react-bootstrap';
import './Navbar.css'
class Navbard extends Component {

    state = {
        value: ""
    };
    /**
    * Handles search box content changes
    * @param {Search box content change event} event 
    */
    handleInput = event => {
        var eventKey = event.key;
        this.setState({
            value: event.target.value
        }, () => {
            if (eventKey === 'Enter') {
                this.props.fetchData(this.props.urlFR + 'section/' + this.state.value)
            }
        });
        this.props.changeDisplayForm(false)
    };

    /**
     * Handles dropdown menu item selection
     * @param {# of selected dropdown menu item} i 
     */
    handleDropDown(i) {
        this.props.fetchData(this.props.urlFR + 'chapters/' + i)
        this.props.changeDisplayForm(false)
    };

    /**
     * Handles request for complete set of rules
     */
    handleCompleteRules() {
        this.props.fetchData(this.props.urlFR + 'section/')
        this.props.changeDisplayForm(false)
    };

    /**
    * Handles filter queries
    * @param {characters in filterbox} event 
    */
    handleFilter = event => {
        this.props.changeDisplayForm(false)


        this.setState({
            value: event.target.value
        }, () => {
            var x = this.state.value.length

            //no empty queries
            if (x > 0) {
                this.props.fetchData(this.props.urlFR + 'filter/' + this.state.value)
            }
        });



    }




    render() {

        return (

            <Navbar fixed="top" bg="light" expand="lg">
                <Navbar.Brand href="#home" onClick={() => {this.handleCompleteRules()  }}>Complete Rules</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link onClick={() => { this.props.changeDisplayForm(true) }} href="#rules">Select new rulebook</Nav.Link>
                        <NavDropdown title="Sections" id="basic-nav-dropdown">
                            <NavDropdown.Item onClick={() => { this.handleDropDown(1) }} href="#chapter_1">1. Game Concepts</NavDropdown.Item>
                            <NavDropdown.Item onClick={() => { this.handleDropDown(2) }} href="#chapter_2">2. Parts of a Card</NavDropdown.Item>
                            <NavDropdown.Item onClick={() => { this.handleDropDown(3) }} href="#chapter_3">3. Card Types</NavDropdown.Item>
                            <NavDropdown.Item onClick={() => { this.handleDropDown(4) }} href="#chapter_4">4. Zones</NavDropdown.Item>
                            <NavDropdown.Item onClick={() => { this.handleDropDown(5) }} href="#chapter_5">5. Turn Structure</NavDropdown.Item>
                            <NavDropdown.Item onClick={() => { this.handleDropDown(6) }} href="#chapter_6">6. Spells, Abilities, and Effects</NavDropdown.Item>
                            <NavDropdown.Item onClick={() => { this.handleDropDown(7) }} href="#chapter_7">7. Additional Rules. Card Types</NavDropdown.Item>
                            <NavDropdown.Item onClick={() => { this.handleDropDown(8) }} href="#chapter_8">8. Multiplayer Rules</NavDropdown.Item>
                            <NavDropdown.Item onClick={() => { this.handleDropDown(9) }} href="#chapter_9">9. Casual Variants</NavDropdown.Item>

                        </NavDropdown>
                    </Nav>
                    <Form style= {{marginLeft: '10px'}} inline>
                        <FormControl type="text" placeholder="Input a rule" onKeyPress={(e) => { e.key === 'Enter' && e.preventDefault(); }} onKeyDown={this.handleInput} onChange={this.handleInput} className="mr-sm-2" />

                        
                    </Form>
                    <Button onClick={() => { this.props.fetchData(this.props.urlFR + 'section/' + this.state.value) }} variant="outline-primary">Get rule</Button>
                    <Form style= {{margin: '10px'}} inline>
                        <FormControl type="text" placeholder="Filter" onChange={this.handleFilter} className="mr-sm-2" />
                        
                    </Form>

                </Navbar.Collapse>
            </Navbar>
        );
    }
}
export default Navbard;