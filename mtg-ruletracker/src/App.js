import React, { Component } from 'react'
import './App.css';
import Navbar from './Navbar'
import Section from './Section'
import 'bootstrap/dist/css/bootstrap.css';
import ChangeParseForm from './ChangeParseForm'

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            Text: 'Loading ',
            diplayform: false,
            url: 'http://localhost:8080/'
        }
        this.fetchData = this.fetchData.bind(this);
        this.changeDisplayForm = this.changeDisplayForm.bind(this);
        this.postUrl = this.postUrl.bind(this);
    }

    /**
     * Determines wether to render ChangeParseForm, or Section
     * @param {true = display form component for changing txtfile, false = display rule contents} bool
     */
    changeDisplayForm(bool) {

        // eslint-disable-next-line eqeqeq
        if (this.state.diplayform != bool) {
            this.setState({ diplayform: bool });
            this.render()
        }


    }
    async fetchData(url) {
        fetch(url)
            .then(function (response) {
                return response.text();
            })
            .then(function (text) {
                let tocSectionOneCount = 0;
                let newText = text.split('\n').map(i => {
                  if (i.substring(0, 2) == '1.'){
                    tocSectionOneCount ++;
                  }
                  console.log(url)
                  if ((tocSectionOneCount < 2 && url === this.state.url + "section/") && i.charAt(1) !== '.') {
                    var x = i.substring(0, 5);
                    return <p style={{ color: 'blue' }} href="" onClick={() => { this.fetchData(this.state.url + "section/" + x) }}>{i}</p>
        
                  } else if ((tocSectionOneCount < 2 && url === this.state.url + "section/") && i.charAt(1) === '.') {
        
                    return <p style={{ color: 'blue' }} href="" onClick={() => { this.fetchData(this.state.url + "chapters/" + i.charAt(0)) }}>{i}</p>
        
                  }
                  else {
                    return <p>{i}</p>
                  }
                });

                this.setState({ Text: newText });
            }.bind(this))
            .catch((error) => {
                console.log(error);
            });
    }





    postUrl(urltoBack, urlToSend, start, end) {

        fetch(urltoBack, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: 'post',
            body: JSON.stringify({url:urlToSend, start:start,end: end})
        }).then(function (response) {
            return response.json();
        }).then(function (data) {
            console.log(data);
        });
    }



    componentDidMount = () => {
        this.fetchData(this.state.url + 'section/');

    }


    render() {

        if (this.state.diplayform == true) {
            return (
                <div  >


                    <ChangeParseForm changeDisplayForm={this.changeDisplayForm} urlFR={this.state.url} postUrl={this.postUrl} fetchData={this.fetchData}></ChangeParseForm>

                    <Navbar changeDisplayForm={this.changeDisplayForm} urlFR={this.state.url} fetchData={this.fetchData} Section={Section}></Navbar>


                </div>
            );
        }
        else {
            return (
                <div >


                    <Navbar changeDisplayForm={this.changeDisplayForm} urlFR={this.state.url} fetchData={this.fetchData} Section={Section}></Navbar>

                    <Section Text={this.state.Text} ></Section>


                </div>
            );
        }
    }
}

export default App;

