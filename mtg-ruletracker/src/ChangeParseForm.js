import React, { Component } from 'react'
import './ChangeParseForm.css'


class ChangeParseForm extends Component {

    state = {
        value: "",
        start: 169,
        end: 5760,
    };

    /**
    * Submits the form data to backend
    * @param {Submit button clicked} event 
    */
    handleClick = event => {
        this.props.postUrl(this.props.urlFR+ 'submitUrl/', this.state.value,this.state.start,this.state.end)
        this.props.changeDisplayForm(false)

    }

    /**
     * Determines start point for processing text
     * @param {int for row to start processing} event 
     */
    handleStartInput = event => {
        let startHere = event.target.value; 
        console.log("Start " + startHere)
        this.setState({ start: startHere });


    }
    /**
     * Determines endpoint for processing text
     * @param {int for row to end processing} event 
     */
    handleEndInput = event => {
        let endtHere = event.target.value; 
        console.log("Start " + endtHere)
        this.setState({ end: endtHere });


    }

    /**
     * Determines url that will be processed in the backend
     * @param {url to fetch text from} event 
     */
    handleInput = event => {
        let str = event.target.value;
        this.setState({ value: str });

    }




    render() {


        return (
            <div className="background-parch">

                <form onSubmit={this.handleSubmit}>
                    <label>
                        Rules start at line: { }
                        <input style = {{ width: '75%'}} type="text" value={this.state.start} onChange={this.handleStartInput} />
                    </label>
                    <br></br>
                    <br></br>
                    <label>
                        Rules end at line: { }
                        <input style = {{ width: '75%'}}  type="text" value={this.state.end} onChange={this.handleEndInput} />
                    </label>
                    <br></br>
                    <br></br>
                    <label>
                        Insert Url to new rules: { }
                        <input style = {{ width: '75%'}}type="text" value={this.state.value} onChange={this.handleInput} />
                    </label>
                    <input type="submit" onClick={() => { this.handleClick() }} value="Submit" />
                </form>

            </div>
        );
    }
}
export default ChangeParseForm;