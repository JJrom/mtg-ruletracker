package com.example.demo;

import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.net.MalformedURLException;
import java.net.URL;
/**
 * Rest controller
 */
@RestController
public class SectionController {

    /**
     * Handles logic for processing and retrieving content in txt file
     */
    public InputProcessor ruleHandler = new InputProcessor();


    public SectionController() throws MalformedURLException {
    }

    /**
     * Starts text handler
     */

    @PostConstruct
    public void init() {
        ruleHandler.processTxt();

    }

    /**
     * Returns rule and referenced rules
     * @param sectionId name of the rule to get
     * @return rule and referenced rules
     */
    @RequestMapping(value="/section/{sectionId}", produces = "text/plain;charset=UTF-8")
    public  String SectionString(@PathVariable String sectionId) {
        return ruleHandler.returnSectionById(sectionId) + ruleHandler.returnRelatedSections(sectionId);
    }

    /**
     * Returns all rules
     * @return complete rules
     */
    @RequestMapping(value="/section/", produces = "text/plain;charset=UTF-8")
    public  String FullList() {
        return ruleHandler.returnCompleteRuleset();
    }

    /**
     * Returns a chapter by int
     * @param chapter int for chapter to get
     * @return chapter of rules
     */

    @RequestMapping(value="/chapters/{chapter}", produces = "text/plain;charset=UTF-8")
    public  String Chapter( @PathVariable int chapter) {
        return ruleHandler.chapters.get(chapter);
    }


    /**
     * Changes the txt file to one at url passed as parameter
     * @param changeB contains starting line, end line and url to rules
     * @return New txt processed
     */

    @PostMapping("/submitUrl/")
    String Submit(@RequestBody urlChangeBody changeB) {
        ruleHandler.setUrl(changeB.url);
        ruleHandler.start = changeB.start;
        ruleHandler.end = changeB.end;
        return ruleHandler.completeString;
    }

    /**
     * Returns String with all rules that contain filterTerm
     * @param filterTerm String to filter results by
     * @return All rules that contain filterTerm
     */
    @RequestMapping(value="/filter/{filterTerm}", produces = "text/plain;charset=UTF-8")
    public  String FilteredString(@PathVariable String filterTerm) {
        return ruleHandler.filteredRules(filterTerm);
    }

    static class urlChangeBody {
        public URL url;
        public int start;
        public int end;
    }

}