package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main class
 */
@SpringBootApplication
public class TxtRuleBackend {

    /**
     * Starts backend
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(TxtRuleBackend.class, args);
    }



}
