package com.example.demo;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

/**
 * Handles logic for processing and relaying useful contents of txt file
 */
public class InputProcessor {

    HashMap<String, Section> allRulesMap = new HashMap<>();
    ArrayList<String> chapters = new ArrayList<>();
    ArrayList<String> allSectionKeys = new ArrayList<>(); // Used for inorder iteration of hashmap, we want the filtered results to be in order.
    String completeString = "";
    String blockCharacters = "!@#$%&*()'+,-–/:;<=>?[]^_`{|}";
    int start = 168;
    int end = 5760;
    URL url = new URL("https://media.wizards.com/2021/downloads/MagicCompRules%2020210419.txt");

    public InputProcessor() throws MalformedURLException {
    }

    /**
     * Processes txt file into usable formats.
     */
    void processTxt() {
        String root = "100.";
        String sectionString = "\n";
        int row = 1;
        int currentSection = 0;
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            String line;
            while (row < start) {
                row++;
                line = in.readLine();
                if ( line.length()>0){
                    if (Character.isDigit(line.charAt(0))){
                        completeString += line + "\n";
                        System.out.println(completeString);
                    }
                }
            }

            while ((line = in.readLine()) != null && row < end) {

                completeString += line + "\n";

                if (line.length() > 0) {

                    // We want to process line if it's a new section marked by a number at the start,
                    // Or if the Section starts with "Example", that needs to be added to previous section.
                    if (Character.isDigit(line.charAt(0)) || (line.length() >= 7 && line.startsWith("Example"))) {


                        //This handles adding sections to chapters arraylist

                        int x = 0;
                        if (Character.isDigit(line.charAt(0))) {
                            x = Integer.parseInt((Character.toString(line.charAt(0))));

                        }
                        if (x == currentSection || !Character.isDigit(line.charAt(0))) {
                            sectionString += "\n" + line + "\n";
                        } else {
                            chapters.add(sectionString);
                            currentSection++;
                            sectionString = "\n" + line;
                        }

                        int i = 0;
                        Section sectionToAdd = new Section(line);
                        String stringKeyForMap = "";

                        // Determines the section key String
                        while (i < line.length()) {
                            Character c = line.charAt(i);
                            if (c == ' ' || blockCharacters.contains(Character.toString(c))) {
                                break;
                            }
                            stringKeyForMap += c;
                            i++;
                        }

                        while (i < line.length()) {

                            String tmp = "";

                            // This checks if line contains valid key for reference section
                            if (Character.isDigit(line.charAt(i))) {
                                int dotcount = 0;
                                while ((i < line.length() && line.charAt(i) != ' ') && ((!blockCharacters.contains(Character.toString(line.charAt(i)))))) {
                                    if (line.charAt(i) == '.') {
                                        dotcount++;
                                        if (dotcount == 2) {
                                            break;
                                        }
                                    }
                                    tmp += line.charAt(i);
                                    i++;
                                }
                            }
                            // If reference section has a valid key, add it to stored sections references.
                            if (isValidRuleSection(tmp)) {
                                if (Character.isDigit(tmp.charAt(tmp.length() - 1))) {
                                    tmp += '.';
                                }
                                sectionToAdd.addSectionRefs(tmp);


                            }
                            i++;
                        }
                        if (isValidRuleSection(stringKeyForMap)) {
                            if (Character.isDigit(stringKeyForMap.charAt(stringKeyForMap.length() - 1))) {
                                stringKeyForMap += '.';
                            }
                            // Store section into hashmap, the key is rule string (Ex, 101.1.)
                            allSectionKeys.add(stringKeyForMap);
                            allRulesMap.put(stringKeyForMap, sectionToAdd);

                            if (stringKeyForMap.substring(0,4).equals( root)) {
                                allRulesMap.get(root).addSectionRefs(stringKeyForMap);

                            } else {
                                root = stringKeyForMap;
                            }
                        }
                    }
                }
                row++;
            }

            in.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        chapters.add(sectionString);
    }

    /**
     * @param searchTerm Term that was searched for in the frontend
     * @return String containing all matching rules
     */
    String filteredRules(String searchTerm) {
        String returnString = "";

        for (String key : allSectionKeys) {
            if (allRulesMap.get(key).getText().contains(searchTerm)) {
                returnString += allRulesMap.get(key).getText() + "\n";
            }

        }

        return returnString;

    }

    /**
     * Checks if a section is valid structure for a section
     *
     * @param keyToCheck String to analyze for validity
     * @return True if is valid section, else false
     */
    boolean isValidRuleSection(String keyToCheck) {
        int countDigits = 0;
        int digitsInARow = 0;
        for (int i = 0; i < keyToCheck.length(); i++) {
            if (Character.isDigit(keyToCheck.charAt(i))) {
                digitsInARow++;
                countDigits++;
            } else {
                digitsInARow = 0;
            }

            //if the string has 4 or more digits in a row without . it doesn't follow rule naming convention
            if (digitsInARow > 3) {
                return false;
            }
        }
        return countDigits >= 3;
    }

    /**
     * @param newUrl url to fetch new txt file from
     */
    void setUrl(URL newUrl) {
        this.url = newUrl;
        allRulesMap.clear();
        allSectionKeys.clear();
        chapters.clear();
        completeString = "";
        processTxt();

    }

    /**
     * @param id name of the rule, ex "101.1."
     * @return String containing text of the rule
     */
    String returnSectionById(String id) {
        if (allRulesMap.containsKey(id)) {
            return allRulesMap.get(id).getText();
        } else {

            return "No rule found. Rules should be formatted like :104.3a for rules ending in letters and 101.1. for ones ending in number, remember the dot! ";
        }
    }

    /**
     * @param sectionKey Section name to get refered sections from
     * @return referenced sections
     */
    String returnRelatedSections(String sectionKey) {
        if (allRulesMap.containsKey(sectionKey) && !allRulesMap.get(sectionKey).getSectionRefs().isEmpty()) {
            Section sectionToGetRefsFrom = (allRulesMap.get(sectionKey));
            String refsString = "\n" + " Related Sections: " + "\n";
            for (String s : sectionToGetRefsFrom.sectionRefs) {
                refsString += allRulesMap.get(s).getText() + "\n";
            }

            return refsString;
        }
        return "";
    }

    /**
     * @return String containing all the rules
     */
    String returnCompleteRuleset() {
        return completeString;
    }

    /**
     * @return HashMap with all rules, key is rule name
     */
    HashMap<String, Section> returnMap() {
        return allRulesMap;
    }
}

