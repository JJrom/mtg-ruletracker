package com.example.demo;

import java.util.ArrayList;
import java.util.HashSet;


/**
 * Object representing rules, contains text and related rule keys
 */
public class Section {

    //Stores the text of the rule in question
    String text;
    //Contains reference to all relevant sections elsewhere in the rules
    ArrayList<String> sectionRefs = new ArrayList<>();

    Section(String text) {
        System.out.println();
        this.text = text;

    }

    /**
     * Ads a section to references
     * @param s section key to add
     */
    void addSectionRefs(String s) {
        sectionRefs.add(s);
    }

    /**
     * Getter for sections text
     * @return String containing sections text
     */
    String getText() {
        System.out.println();
        return this.text;

    }

    /**
     * Returns sections that this section referred to
     * @return HashSet containing referenced sections
     */
    ArrayList<String> getSectionRefs() {
        return sectionRefs;
    }

}